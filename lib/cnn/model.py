import numpy as np
import os.path

from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist

from pprint import pprint

class Model:
    def __init__(self, model_file = "model.h5"):
        self.model_file = model_file
        self.model = self.getModel(self.model_file)

    def prepareData(self):
        # load data
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        # Preprocess input data
        x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
        x_train = x_train.astype('float32')
        x_train /= 255

        cat_y_train = np_utils.to_categorical(y_train, 10)

        x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)
        x_test = x_test.astype('float32')
        x_test /= 255

        cat_y_test = np_utils.to_categorical(y_test, 10)

        return ((x_train, cat_y_train), (x_test, cat_y_test))

    def getModel(self, model_file):
        if not os.path.exists(model_file):
            # Define model architecture
            model = Sequential()

            model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(28, 28, 1)))
            model.add(Convolution2D(32, 3, 3, activation='relu'))
            model.add(MaxPooling2D(pool_size=(2, 2)))
            model.add(Dropout(0.25))

            model.add(Flatten())
            model.add(Dense(128, activation='relu'))
            model.add(Dropout(0.5))
            model.add(Dense(10, activation='softmax'))

            # Compile model
            model.compile(loss='categorical_crossentropy',
                          optimizer='adam',
                          metrics=['accuracy'])

            return model
        else:
            return load_model(model_file)

    def train(self, batch_size = 32, nb_epoch = 10):
        (x_train, y_train), (_, _) = self.prepareData()

        self.model.fit(x_train, y_train, batch_size = batch_size, nb_epoch = nb_epoch, verbose = 1)
        self.model.save(self.model_file)

    def evaluate(self, x_test, cat_y_test):
        return self.model.evaluate(x_test, cat_y_test, verbose = 0)

    def predict(self, x_test):
        return self.model.predict(x_test)
