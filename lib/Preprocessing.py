import sys
import os
import cv2
import h5py
import json
import numpy as np
from pprint import pprint


class Preprocessing:
    tmp_f_cache = None

    def __init__(self, base_path):
        self.base_path = base_path
        self.cache_path = os.path.join(base_path, "data/cache_data.hdf5")

    def load_data_from_cache(self):
        with h5py.File(self.cache_path, "r") as f_cache:
            print('Load data from cache file...')

            return [json.loads(item) for item in f_cache["."]["data"][()]], \
                json.loads(f_cache["."]["count"][()]), \
                json.loads(f_cache["."]["mapping"][()])

    def load_data_from_disk(self, img_path, input_path):
        found_bg = False
        all_imgs = {}
        classes_count = {}
        class_mapping = {}
        i = 1

        with open(input_path, 'r') as f, h5py.File(self.cache_path, "w") as f_cache:
            print('Parsing annotation files...')

            for line in f:
                # Print process
                sys.stdout.write('\r' + 'idx=' + str(i))
                i += 1

                line_split = line.strip().split(',')

                # Make sure the info saved in annotation file matching the
                # format (path_filename, x1, y1, x2, y2, class_name)
                # Note:
                #   One path_filename might has several classes (class_name)
                #   x1, y1, x2, y2 are the pixel value of the origial image, not the ratio value
                #   (x1, y1) top left coordinates; (x2, y2) bottom right coordinates
                #   x1,y1-------------------
                #   |                       |
                #   |                       |
                #   |                       |
                #   |                       |
                #   ---------------------x2,y2
                (filename, x1, y1, x2, y2, class_name) = line_split

                if class_name not in classes_count:
                    classes_count[class_name] = 1
                else:
                    classes_count[class_name] += 1

                if class_name not in class_mapping:
                    if class_name == 'bg' and not found_bg:
                        print('Found class name with special name bg. Will be treated as a background region (this is '
                              'usually for hard negative mining).')
                        found_bg = True

                    class_mapping[class_name] = len(class_mapping)

                filepath = os.path.join(self.base_path, img_path, filename)

                if filepath not in all_imgs:
                    all_imgs[filepath] = {}
                    img = cv2.imread(filepath)
                    (rows, cols) = img.shape[:2]

                    all_imgs[filepath]['filepath'] = filepath
                    all_imgs[filepath]['width'] = cols
                    all_imgs[filepath]['height'] = rows
                    all_imgs[filepath]['bboxes'] = []

                    # if np.random.randint(0,6) > 0:
                    #   all_imgs[filepath]['imageset'] = 'trainval'
                    # else:
                    #   all_imgs[filepath]['imageset'] = 'test'

                all_imgs[filepath]['bboxes'].append({
                    'class': class_name,
                    'x1': int(x1),
                    'x2': int(x2),
                    'y1': int(y1),
                    'y2': int(y2),
                })

            all_data = []

            for key in all_imgs:
                all_data.append(all_imgs[key])

            # make sure the bg class is last in the list
            if found_bg and class_mapping['bg'] != len(class_mapping) - 1:
                key_to_switch = [key for key in class_mapping.keys() if class_mapping[key] == len(class_mapping) - 1]
                val_to_switch = class_mapping['bg']
                class_mapping['bg'] = len(class_mapping) - 1
                class_mapping[key_to_switch[0]] = val_to_switch

            # save into cachepython
            f_cache.create_dataset("data", data=np.array([json.dumps(item) for item in all_data], dtype='S'))
            f_cache.create_dataset("count", data=json.dumps(classes_count))
            f_cache.create_dataset("mapping", data=json.dumps(class_mapping))

            return all_data, classes_count, class_mapping

    def get_data(self, img_path, input_path):
        # Parse the data from annotation file
        # Args:
        #     input_path: annotation file path
        # Returns:
        #     all_data: list(filepath, width, height, list(bboxes))
        #     classes_count: dict{key:class_name, value:count_num}
        #         e.g. {'Car': 2383, 'Mobile phone': 1108, 'Person': 3745}
        #     class_mapping: dict{key:class_name, value: idx}
        #         e.g. {'Car': 0, 'Mobile phone': 1, 'Person': 2}
        if os.path.exists(self.cache_path):
            return self.load_data_from_cache()
        else:
            return self.load_data_from_disk(img_path, input_path)
