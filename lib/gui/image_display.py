import numpy as np
import tkinter as tk
from PIL import Image, ImageTk
import cv2

from lib.gui.status import Status
from lib.cnn.model import Model
from pprint import pprint

class ImageDisplay:
    def __init__(self, parent):
        self.parent = parent
        self.model = Model("data/model.h5")

    def render(self):
        # Video Player
        self.vidPlayer = tk.Label(self.parent)
        self.vidPlayer.grid(row = 0, column = 0)

        # Status Frame
        self.statFrame = tk.LabelFrame(self.parent, text="Result")
        self.statFrame.grid(row = 10, column = 0, padx = 10, pady = 2)

        self.status = Status(self.statFrame)
        self.status.render()

    def predict(self):
        _, frame = self.video.read()

        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        img = Image.fromarray(cv2image)
        imgtk = ImageTk.PhotoImage(image = img)

        self.vidPlayer.imgtk = imgtk
        self.vidPlayer.configure(image = imgtk)
        self.vidPlayer.after(10, self.render_frame)

        img_input = cv2.resize(frame, (28, 28));
        img_input = cv2.cvtColor(img_input, cv2.COLOR_BGR2GRAY)

        prediction = self.model.predict(img_input.reshape(1, 28, 28, 1))
        self.status.updateValue(prediction.argmax())
