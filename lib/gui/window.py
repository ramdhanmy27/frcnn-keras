import tkinter as tk
from PIL import Image, ImageTk

from lib.gui.image_display import ImageDisplay

class Window:
    def __init__(self, title):
        self.window = tk.Tk()
        self.window.wm_title(title)
        self.window.config(background = "#FFFFFF")

    def render(self):
        # Control Button
        btn_frame = ttk.Frame(self.window, padding = "0 5 0 5")
        btn_frame.grid(column = 0, row = 0, padx = 5, columnspan = 3)

        btn_sel_img = tk.Button(btn_frame, text = "Select Image", command = self.select_image)
        btn_sel_img.grid(column = 0, row = 0, sticky = (tk.W), padx = 5)

        # loading frame
        self.frame_loading = ttk.Frame(self.window, padding = "10 10 10 10")
        self.frame_loading.grid(column = 1, row = 0, sticky = (tk.W, tk.E, tk.N, tk.S), padx = 5, columnspan = 3)
        loading_label = tk.Label(self.frame_loading, text = "Please Wait", anchor="center")
        loading_label.pack()
        self.frame_loading.grid_remove()

        # image frame
        self.img_frame = tk.Frame(self.window, width = 600, height = 500)
        self.img_frame.grid(row = 0, column = 0, padx = 10, pady = 2)
        self.img_frame.grid_remove()

        self.img_display = ImageDisplay(self.img_frame)
        self.img_display.render()

    def select_image(self):
        filetypes = [("Image files", "*.jpg *.png *.gif")]
        img_file = filedialog.askopenfilename(filetypes = filetypes)

        if img_file:
            self.display(img_file)

    def loading(self, is_loading = True):
        if is_loading:
            self.frame_loading.grid()
            self.img_frame.grid_remove()
        else:
            self.frame_loading.grid_remove()
            self.img_frame.grid()

        self.window.update()

    def display(self, img_file):
        self.img_display.display(img_file)
        self.img_display.predict()

    def run(self):
        self.window.mainloop()
