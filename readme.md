# Overview
## Usage

```shell script
python main.py [test|train] [base_path]
```

### File Structure
```
data/
  test/                     # test images
  train/                    # train images
  annotation.txt            # train annotation
  model_frcnn_vgg.hdf5      # pretrain model
  model_vgg_config.pickle     # configuration
  record.csv
  test_annotation.txt       # test annotation
lib/
  gui/
model/
  layer/
test.py
train.py
```

# Task
## todo
[] download image
