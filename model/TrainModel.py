import random
import pprint
import time
import numpy as np
import pickle
import cv2
from matplotlib import pyplot as plt
import pandas as pd
import os

from keras import backend
from keras.optimizers import Adam
from keras.layers import Input
from keras.models import Model
from keras.utils import generic_utils

from lib.Config import Config
from lib.Preprocessing import Preprocessing
from model.VGG16 import VGG16
from model.layer.Classifier import Classifier
from model.layer.RPN import RPN


class TrainModel:
    def __init__(self, base_path):
        self.rpn = RPN()
        self.model = VGG16()
        self.classifier = Classifier()
        self.preprocessing = Preprocessing(base_path)

        self.base_path = base_path
        self.train_path = os.path.join(self.base_path, 'data/annotation.txt')

        # Number of RoIs to process at once.
        self.num_rois = 4

        # Augmentation flag
        self.horizontal_flips = True  # Augment with horizontal flips in training.
        self.vertical_flips = True  # Augment with vertical flips in training.
        self.rot_90 = True  # Augment with 90 degree rotations in training.

        self.base_weight_path = os.path.join(self.base_path, 'data/vgg16_weights_tf_dim_ordering_tf_kernels.h5')
        self.output_weight_path = os.path.join(self.base_path, 'data/model_frcnn_vgg.hdf5')
        self.config_output_filename = os.path.join(self.base_path, 'data/model_vgg_config.pickle')

        # Record data (used to save the losses, classification accuracy and mean average precision)
        self.record_path = os.path.join(self.base_path, 'data/record.csv')

    def make_config(self, class_mapping):
        c = Config()
        c.use_horizontal_flips = self.horizontal_flips
        c.use_vertical_flips = self.vertical_flips
        c.rot_90 = self.rot_90
        c.num_rois = self.num_rois

        c.base_net_weights = self.base_weight_path
        c.model_path = self.output_weight_path
        c.record_path = self.record_path

        c.class_mapping = class_mapping

        # Save the configuration
        with open(self.config_output_filename, 'wb') as config_f:
            pickle.dump(c, config_f)

            print('Config has been written to {}, and can be loaded when testing to ensure correct '
                  'results'.format(self.config_output_filename))

        return c

    def load_data(self):
        # This step will spend some time to load the data
        st = time.time()
        train_imgs, classes_count, class_mapping = self.preprocessing.get_data("data/train", self.train_path)

        print('\nSpend %0.2f mins to load the data' % ((time.time() - st) / 60))

        return train_imgs, classes_count, class_mapping

    def explore(self, c, data_gen_train):
        """#### Explore 'data_gen_train'
        data_gen_train is an **generator**, so we get the data by calling **next(data_gen_train)**
        """
        x, y, image_data, debug_img, debug_num_pos = next(data_gen_train)

        print('Original image: height=%d width=%d' % (image_data['height'], image_data['width']))
        print('Resized image:  height=%d width=%d c.im_size=%d' % (x.shape[1], x.shape[2], c.im_size))
        print('Feature map size: height=%d width=%d c.rpn_stride=%d' % (y[0].shape[1], y[0].shape[2], c.rpn_stride))
        print(x.shape)
        print(str(len(y)) + " includes 'y_rpn_cls' and 'y_rpn_regr'")
        print('Shape of y_rpn_cls {}'.format(y[0].shape))
        print('Shape of y_rpn_regr {}'.format(y[1].shape))
        print(image_data)
        print('Number of positive anchors for this image: %d' % debug_num_pos)

        if debug_num_pos == 0:
            gt_x1 = int(image_data['bboxes'][0]['x1'] * (x.shape[2] / image_data['height']))
            gt_x2 = int(image_data['bboxes'][0]['x2'] * (x.shape[2] / image_data['height']))
            gt_y1 = int(image_data['bboxes'][0]['y1'] * (x.shape[1] / image_data['width']))
            gt_y2 = int(image_data['bboxes'][0]['y2'] * (x.shape[1] / image_data['width']))

            img = debug_img.copy()
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            color = (0, 255, 0)
            cv2.putText(img, 'gt bbox', (gt_x1, gt_y1 - 5), cv2.FONT_HERSHEY_DUPLEX, 0.7, color, 1)
            cv2.rectangle(img, (gt_x1, gt_y1), (gt_x2, gt_y2), color, 2)
            cv2.circle(img, (int((gt_x1 + gt_x2) / 2), int((gt_y1 + gt_y2) / 2)), 3, color, -1)

            plt.grid()
            plt.imshow(img)
            plt.show()
        else:
            cls = y[0][0]
            pos_cls = np.where(cls == 1)
            print(pos_cls)
            regr = y[1][0]
            pos_regr = np.where(regr == 1)
            print(pos_regr)
            print('y_rpn_cls for possible pos anchor: {}'.format(cls[pos_cls[0][0], pos_cls[1][0], :]))
            print('y_rpn_regr for positive anchor: {}'.format(regr[pos_regr[0][0], pos_regr[1][0], :]))

            gt_x1 = int(image_data['bboxes'][0]['x1'] * (x.shape[2] / image_data['width']))
            gt_x2 = int(image_data['bboxes'][0]['x2'] * (x.shape[2] / image_data['width']))
            gt_y1 = int(image_data['bboxes'][0]['y1'] * (x.shape[1] / image_data['height']))
            gt_y2 = int(image_data['bboxes'][0]['y2'] * (x.shape[1] / image_data['height']))

            img = debug_img.copy()
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            color = (0, 255, 0)
            # cv2.putText(img, 'gt bbox', (gt_x1, gt_y1-5), cv2.FONT_HERSHEY_DUPLEX, 0.7, color, 1)
            cv2.rectangle(img, (gt_x1, gt_y1), (gt_x2, gt_y2), color, 2)
            cv2.circle(img, (int((gt_x1 + gt_x2) / 2), int((gt_y1 + gt_y2) / 2)), 3, color, -1)

            # Add text
            text_label = 'gt bbox'
            (retval, baseLine) = cv2.getTextSize(text_label, cv2.FONT_HERSHEY_COMPLEX, 0.5, 1)
            text_org = (gt_x1, gt_y1 + 5)

            cv2.rectangle(
                img,
                (text_org[0] - 5, text_org[1] + baseLine - 5),
                (text_org[0] + retval[0] + 5, text_org[1] - retval[1] - 5),
                (0, 0, 0),
                2
            )
            cv2.rectangle(
                img,
                (text_org[0] - 5, text_org[1] + baseLine - 5),
                (text_org[0] + retval[0] + 5, text_org[1] - retval[1] - 5),
                (255, 255, 255),
                -1
            )
            cv2.putText(img, text_label, text_org, cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 0), 1)

            # Draw positive anchors according to the y_rpn_regr
            for i in range(debug_num_pos):
                color = (100 + i * (155 / 4), 0, 100 + i * (155 / 4))

                idx = pos_regr[2][i * 4] / 4
                anchor_size = c.anchor_box_scales[int(idx / 3)]
                anchor_ratio = c.anchor_box_ratios[2 - int((idx + 1) % 3)]

                center = (pos_regr[1][i * 4] * c.rpn_stride, pos_regr[0][i * 4] * c.rpn_stride)
                print('Center position of positive anchor: ', center)
                cv2.circle(img, center, 3, color, -1)
                anc_w, anc_h = anchor_size * anchor_ratio[0], anchor_size * anchor_ratio[1]
                cv2.rectangle(img, (center[0] - int(anc_w / 2), center[1] - int(anc_h / 2)),
                              (center[0] + int(anc_w / 2), center[1] + int(anc_h / 2)), color, 2)
                # cv2.putText(
                #     img, 'pos anchor bbox '+str(i+1),
                #     (center[0]-int(anc_w/2), center[1]-int(anc_h/2)-5),
                #     cv2.FONT_HERSHEY_DUPLEX, 0.5, color, 1
                # )

        print('Green bboxes is ground-truth bbox. Others are positive anchors')
        plt.figure(figsize=(8, 8))
        plt.grid()
        plt.imshow(img)
        plt.show()

    def build_model(self, c, classes_count):
        """#### Build the model"""
        input_shape_img = (None, None, 3)

        img_input = Input(shape=input_shape_img)
        roi_input = Input(shape=(None, 4))

        # define the base network (VGG here, can be Resnet50, Inception, etc)
        shared_layers = self.model.nn_base(img_input, trainable=True)

        # define the RPN, built on the base layers
        num_anchors = len(c.anchor_box_scales) * len(c.anchor_box_ratios)  # 9
        rpn = self.rpn.rpn_layer(shared_layers, num_anchors)

        classifier = self.classifier.classifier_layer(
            shared_layers, roi_input, c.num_rois, nb_classes=len(classes_count)
        )

        model_rpn = Model(img_input, rpn[:2])
        model_classifier = Model([img_input, roi_input], classifier)

        # this is a model that holds both the RPN and the classifier, used to load/save weights for the models
        model_all = Model([img_input, roi_input], rpn[:2] + classifier)

        # Because the google colab can only run the session several hours one time (then you need to connect again),
        # we need to save the model and load the model to continue training
        if not os.path.isfile(c.model_path):
            # If this is the begin of the training, load the pre-traind base network such as vgg-16
            try:
                print('This is the first time of your training')
                print('loading weights from {}'.format(c.base_net_weights))
                model_rpn.load_weights(c.base_net_weights, by_name=True)
                model_classifier.load_weights(c.base_net_weights, by_name=True)
            except:
                print('Could not load pretrained model weights. Weights can be found in the keras application folder \
                    https://github.com/fchollet/keras/tree/master/keras/applications')

            # Create the record.csv file to record losses, acc and mAP
            record_df = pd.DataFrame(
                columns=['mean_overlapping_bboxes', 'class_acc', 'loss_rpn_cls', 'loss_rpn_regr', 'loss_class_cls',
                         'loss_class_regr', 'curr_loss', 'elapsed_time', 'mAP']
            )
        else:
            # If this is a continued training, load the trained model from before
            print('Continue training based on previous trained model')
            print('Loading weights from {}'.format(c.model_path))
            model_rpn.load_weights(c.model_path, by_name=True)
            model_classifier.load_weights(c.model_path, by_name=True)

            # Load the records
            record_df = pd.read_csv(self.record_path)

            # r_mean_overlapping_bboxes = record_df['mean_overlapping_bboxes']
            # r_class_acc = record_df['class_acc']
            # r_loss_rpn_cls = record_df['loss_rpn_cls']
            # r_loss_rpn_regr = record_df['loss_rpn_regr']
            # r_loss_class_cls = record_df['loss_class_cls']
            # r_loss_class_regr = record_df['loss_class_regr']
            # r_curr_loss = record_df['curr_loss']
            # r_elapsed_time = record_df['elapsed_time']
            # r_map = record_df['mAP']

            print('Already train %dK batches' % (len(record_df)))

        optimizer = Adam(lr=1e-5)
        optimizer_classifier = Adam(lr=1e-5)

        model_rpn.compile(
            optimizer=optimizer,
            loss=[
                self.rpn.rpn_loss_cls(num_anchors),
                self.rpn.rpn_loss_regr(num_anchors)
            ]
        )

        model_classifier.compile(
            optimizer=optimizer_classifier,
            loss=[
                self.classifier.class_loss_cls,
                self.classifier.class_loss_regr(len(classes_count) - 1)
            ],
            metrics={'dense_class_{}'.format(len(classes_count)): 'accuracy'}
        )

        model_all.compile(optimizer='sgd', loss='mae')

        return record_df, model_rpn, model_classifier, model_all

    def show_graph(self, record_df, r_epochs):
        plt.figure(figsize=(15, 5))
        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, r_epochs), record_df['mean_overlapping_bboxes'], 'r')
        plt.title('mean_overlapping_bboxes')
        plt.subplot(1, 2, 2)
        plt.plot(np.arange(0, r_epochs), record_df['class_acc'], 'r')
        plt.title('class_acc')
        plt.show()

        plt.figure(figsize=(15, 5))
        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, r_epochs), record_df['loss_rpn_cls'], 'r')
        plt.title('loss_rpn_cls')
        plt.subplot(1, 2, 2)
        plt.plot(np.arange(0, r_epochs), record_df['loss_rpn_regr'], 'r')
        plt.title('loss_rpn_regr')
        plt.show()

        plt.figure(figsize=(15, 5))
        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, r_epochs), record_df['loss_class_cls'], 'r')
        plt.title('loss_class_cls')
        plt.subplot(1, 2, 2)
        plt.plot(np.arange(0, r_epochs), record_df['loss_class_regr'], 'r')
        plt.title('loss_class_regr')
        plt.show()

        plt.plot(np.arange(0, r_epochs), record_df['curr_loss'], 'r')
        plt.title('total_loss')
        plt.show()

        # plt.figure(figsize=(15,5))
        # plt.subplot(1,2,1)
        # plt.plot(np.arange(0, r_epochs), record_df['curr_loss'], 'r')
        # plt.title('total_loss')
        # plt.subplot(1,2,2)
        # plt.plot(np.arange(0, r_epochs), record_df['elapsed_time'], 'r')
        # plt.title('elapsed_time')
        # plt.show()

        # plt.title('loss')
        # plt.plot(np.arange(0, r_epochs), record_df['loss_rpn_cls'], 'b')
        # plt.plot(np.arange(0, r_epochs), record_df['loss_rpn_regr'], 'g')
        # plt.plot(np.arange(0, r_epochs), record_df['loss_class_cls'], 'r')
        # plt.plot(np.arange(0, r_epochs), record_df['loss_class_regr'], 'c')
        # # plt.plot(np.arange(0, r_epochs), record_df['curr_loss'], 'm')
        # plt.show()

    def train(self, epoch_length=1000, num_epochs=40, iter_num=0):
        # classes_count: {'Car': 2383, 'Mobile phone': 1108, 'Person': 3745, 'bg': 0}
        # class_mapping: {'Person': 0, 'Car': 1, 'Mobile phone': 2, 'bg': 3}
        train_imgs, classes_count, class_mapping = self.load_data()

        if 'bg' not in classes_count:
            classes_count['bg'] = 0
            class_mapping['bg'] = len(class_mapping)

        print('Training images per class:')
        pprint.pprint(classes_count)
        print('Num classes (including bg) = {}'.format(len(classes_count)))
        print(class_mapping)

        c = self.make_config(class_mapping)

        # Shuffle the images with seed
        random.seed(1)
        random.shuffle(train_imgs)
        print('Num train samples (images) {}'.format(len(train_imgs)))

        # Get train data generator which generate x, y, image_data
        data_gen_train = self.rpn.get_anchor_gt(train_imgs, c, self.model.get_img_output_length, mode='train')

        self.explore(c, data_gen_train)
        record_df, model_rpn, model_classifier, model_all = self.build_model(c, classes_count)

        # Training setting
        total_epochs = len(record_df)
        r_epochs = len(record_df)

        total_epochs += num_epochs

        losses = np.zeros((epoch_length, 5))
        rpn_accuracy_rpn_monitor = []
        rpn_accuracy_for_epoch = []

        if len(record_df) == 0:
            best_loss = np.Inf
        else:
            best_loss = np.min(record_df['curr_loss'])

        print(len(record_df))

        # Start Training
        start_time = time.time()

        for epoch_num in range(num_epochs):
            progbar = generic_utils.Progbar(epoch_length)
            r_epochs += 1
            print('Epoch {}/{}'.format(r_epochs, total_epochs))

            while True:
                try:
                    if len(rpn_accuracy_rpn_monitor) == epoch_length and c.verbose:
                        mean_overlapping_bboxes = float(sum(rpn_accuracy_rpn_monitor)) / len(rpn_accuracy_rpn_monitor)
                        rpn_accuracy_rpn_monitor = []

                        print('Average number of overlapping bounding boxes from RPN = {} for {} previous '
                              'iterations'.format(mean_overlapping_bboxes, epoch_length))

                        if mean_overlapping_bboxes == 0:
                            print('RPN is not producing bounding boxes that overlap the ground truth boxes. Check RPN '
                                  'settings or keep training.')

                    # Generate x (x_img) and label y ([y_rpn_cls, y_rpn_regr])
                    x, y, img_data, debug_img, debug_num_pos = next(data_gen_train)

                    # Train rpn model and get loss value [_, loss_rpn_cls, loss_rpn_regr]
                    loss_rpn = model_rpn.train_on_batch(x, y)

                    # Get predicted rpn from rpn model [rpn_cls, rpn_regr]
                    p_rpn = model_rpn.predict_on_batch(x)

                    # r: bboxes (shape=(300,4))
                    # Convert rpn layer to roi bboxes
                    r = self.rpn.rpn_to_roi(
                        p_rpn[0],
                        p_rpn[1],
                        c,
                        backend.image_dim_ordering(),
                        use_regr=True,
                        overlap_thresh=0.7,
                        max_boxes=300
                    )

                    # note: calc_iou converts from (x1,y1,x2,y2) to (x,y,w,h) format
                    # x2: bboxes that iou > c.classifier_min_overlap for all gt bboxes in 300 non_max_suppression bboxes
                    # y1: one hot code for bboxes from above => x_roi (x)
                    # y2: corresponding labels and corresponding gt bboxes
                    x2, y1, y2, ious = self.rpn.calc_iou(r, img_data, c, class_mapping)

                    # If x2 is None means there are no matching bboxes
                    if x2 is None:
                        rpn_accuracy_rpn_monitor.append(0)
                        rpn_accuracy_for_epoch.append(0)
                        continue

                    # Find out the positive anchors and negative anchors
                    neg_samples = np.where(y1[0, :, -1] == 1)
                    pos_samples = np.where(y1[0, :, -1] == 0)

                    if len(neg_samples) > 0:
                        neg_samples = neg_samples[0]
                    else:
                        neg_samples = []

                    if len(pos_samples) > 0:
                        pos_samples = pos_samples[0]
                    else:
                        pos_samples = []

                    rpn_accuracy_rpn_monitor.append(len(pos_samples))
                    rpn_accuracy_for_epoch.append((len(pos_samples)))

                    if c.num_rois > 1:
                        # If number of positive anchors is larger than 4//2 = 2, randomly choose 2 pos samples
                        if len(pos_samples) < c.num_rois // 2:
                            selected_pos_samples = pos_samples.tolist()
                        else:
                            selected_pos_samples = np.random.choice(
                                pos_samples, c.num_rois // 2, replace=False
                            ).tolist()

                        # Randomly choose (num_rois - num_pos) neg samples
                        try:
                            selected_neg_samples = np.random.choice(
                                neg_samples, c.num_rois - len(selected_pos_samples), replace=False
                            ).tolist()
                        except:
                            selected_neg_samples = np.random.choice(
                                neg_samples, c.num_rois - len(selected_pos_samples), replace=True
                            ).tolist()

                        # Save all the pos and neg samples in sel_samples
                        sel_samples = selected_pos_samples + selected_neg_samples
                    else:
                        # in the extreme case where num_rois = 1, we pick a random pos or neg sample
                        # selected_pos_samples = pos_samples.tolist()
                        # selected_neg_samples = neg_samples.tolist()

                        if np.random.randint(0, 2):
                            sel_samples = random.choice(neg_samples)
                        else:
                            sel_samples = random.choice(pos_samples)

                    # training_data: [x, x2[:, sel_samples, :]]
                    # labels: [y1[:, sel_samples, :], y2[:, sel_samples, :]]
                    #  x                     => img_data resized image
                    #  x2[:, sel_samples, :] => num_rois (4 in here) bboxes which contains selected neg and pos
                    #  y1[:, sel_samples, :] => one hot encode for num_rois bboxes which contains selected neg and pos
                    #  y2[:, sel_samples, :] => labels and gt bboxes for num_rois bboxes which contains selected
                    #                           neg and pos
                    loss_class = model_classifier.train_on_batch(
                        [x, x2[:, sel_samples, :]],
                        [y1[:, sel_samples, :], y2[:, sel_samples, :]]
                    )

                    losses[iter_num, 0] = loss_rpn[1]
                    losses[iter_num, 1] = loss_rpn[2]

                    losses[iter_num, 2] = loss_class[1]
                    losses[iter_num, 3] = loss_class[2]
                    losses[iter_num, 4] = loss_class[3]

                    iter_num += 1

                    progbar.update(
                        iter_num,
                        [
                            ('rpn_cls', np.mean(losses[:iter_num, 0])),
                            ('rpn_regr', np.mean(losses[:iter_num, 1])),
                            ('final_cls', np.mean(losses[:iter_num, 2])),
                            ('final_regr', np.mean(losses[:iter_num, 3])),
                        ]
                    )

                    if iter_num == epoch_length:
                        loss_rpn_cls = np.mean(losses[:, 0])
                        loss_rpn_regr = np.mean(losses[:, 1])
                        loss_class_cls = np.mean(losses[:, 2])
                        loss_class_regr = np.mean(losses[:, 3])
                        class_acc = np.mean(losses[:, 4])

                        mean_overlapping_bboxes = float(sum(rpn_accuracy_for_epoch)) / len(rpn_accuracy_for_epoch)
                        rpn_accuracy_for_epoch = []

                        if c.verbose:
                            print('Mean number of bounding boxes from RPN overlapping ground truth boxes: {}'.format(
                                mean_overlapping_bboxes
                            ))
                            print('Classifier accuracy for bounding boxes from RPN: {}'.format(class_acc))
                            print('Loss RPN classifier: {}'.format(loss_rpn_cls))
                            print('Loss RPN regression: {}'.format(loss_rpn_regr))
                            print('Loss Detector classifier: {}'.format(loss_class_cls))
                            print('Loss Detector regression: {}'.format(loss_class_regr))
                            print('Total loss: {}'.format(
                                loss_rpn_cls + loss_rpn_regr + loss_class_cls + loss_class_regr
                            ))
                            print('Elapsed time: {}'.format(time.time() - start_time))

                            elapsed_time = (time.time() - start_time) / 60

                        curr_loss = loss_rpn_cls + loss_rpn_regr + loss_class_cls + loss_class_regr
                        iter_num = 0
                        start_time = time.time()

                        if curr_loss < best_loss:
                            if c.verbose:
                                print('Total loss decreased from {} to {}, saving weights'.format(best_loss, curr_loss))

                            best_loss = curr_loss
                            model_all.save_weights(c.model_path)

                        new_row = {
                            'mean_overlapping_bboxes': round(mean_overlapping_bboxes, 3),
                            'class_acc': round(record_df['class_acc'], 3),
                            'loss_rpn_cls': round(record_df['loss_rpn_cls'], 3),
                            'loss_rpn_regr': round(record_df['loss_rpn_regr'], 3),
                            'loss_class_cls': round(record_df['loss_class_cls'], 3),
                            'loss_class_regr': round(record_df['loss_class_regr'], 3),
                            'curr_loss': round(record_df['curr_loss'], 3),
                            'elapsed_time': round(record_df['elapsed_time'], 3),
                            'mAP': round(record_df['mAP'], 3)
                        }

                        record_df = record_df.append(new_row, ignore_index=True)
                        record_df.to_csv(self.record_path, index=0)
                        break
                except Exception as e:
                    print('Exception: {}'.format(e))
                    continue

        print('Training complete, exiting.')
        self.show_graph(record_df, r_epochs)
