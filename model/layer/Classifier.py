from keras import backend
from keras.layers import Flatten, Dense, Dropout, TimeDistributed
from keras.objectives import categorical_crossentropy

from .RoiPoolingConv import RoiPoolingConv


class Classifier:
    epsilon = 1e-4
    lambda_cls_regr = 1.0
    lambda_cls_class = 1.0

    def classifier_layer(self, base_layers, input_rois, num_rois, nb_classes = 4):
        """Create a classifier layer

        Args:
            base_layers: vgg
            input_rois: `(1,num_rois,4)` list of rois, with ordering (x,y,w,h)
            num_rois: number of rois to be processed in one time (4 in here)
            nb_classes:

        Returns:
            list(out_class, out_regr)
            out_class: classifier layer output
            out_regr: regression layer output
        """

        input_shape = (num_rois, 7, 7, 512)
        pooling_regions = 7

        # out_roi_pool.shape = (1, num_rois, channels, pool_size, pool_size)
        # num_rois (4) 7x7 roi pooling
        out_roi_pool = RoiPoolingConv(pooling_regions, num_rois)([base_layers, input_rois])

        # Flatten the convlutional layer and connected to 2 FC and 2 dropout
        out = TimeDistributed(Flatten(name='flatten'))(out_roi_pool)
        out = TimeDistributed(Dense(4096, activation='relu', name='fc1'))(out)
        out = TimeDistributed(Dropout(0.5))(out)
        out = TimeDistributed(Dense(4096, activation='relu', name='fc2'))(out)
        out = TimeDistributed(Dropout(0.5))(out)

        # There are two output layer
        # out_class: softmax acivation function for classify the class name of the object
        # out_regr: linear activation function for bboxes coordinates regression
        out_class = TimeDistributed(
            Dense(nb_classes, activation='softmax', kernel_initializer='zero'),
            name='dense_class_{}'.format(nb_classes)
        )(out)

        # note: no regression target for bg class
        out_regr = TimeDistributed(
            Dense(4 * (nb_classes-1), activation='linear', kernel_initializer='zero'),
            name='dense_regress_{}'.format(nb_classes)
        )(out)

        return [out_class, out_regr]

    def class_loss_regr(self, num_classes):
        """Loss function for rpn regression
        Args:
            num_classes: number of anchors (9 in here)
        Returns:
            Smooth L1 loss function
                               0.5*x*x (if x_abs < 1)
                               x_abx - 0.5 (otherwise)
        """
        def class_loss_regr_fixed_num(y_true, y_pred):
            x = y_true[:, :, 4*num_classes:] - y_pred
            x_abs = backend.abs(x)
            x_bool = backend.cast(backend.less_equal(x_abs, 1.0), 'float32')
            _sum = backend.sum(y_true[:, :, :4*num_classes] * (x_bool * (0.5 * x * x) + (1 - x_bool) * (x_abs - 0.5)))

            return self.lambda_cls_regr * _sum / backend.sum(self.epsilon + y_true[:, :, :4*num_classes])

        return class_loss_regr_fixed_num

    def class_loss_cls(self, y_true, y_pred):
        return self.lambda_cls_class * backend.mean(categorical_crossentropy(y_true[0, :, :], y_pred[0, :, :]))
