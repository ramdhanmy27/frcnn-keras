import time
import numpy as np
import pickle
import cv2
from matplotlib import pyplot as plt
import pandas as pd
import os

from sklearn.metrics import average_precision_score

from keras import backend
from keras.layers import Input
from keras.models import Model

from lib.Preprocessing import Preprocessing
from model.VGG16 import VGG16
from model.layer.Classifier import Classifier
from model.layer.RPN import RPN


class TestModel:
    def __init__(self, base_path):
        self.rpn = RPN()
        self.model = VGG16()
        self.classifier = Classifier()

        self.base_path = base_path
        self.test_path = os.path.join(base_path, 'data/test_annotation.txt')
        self.test_base_path = os.path.join(base_path, 'data/test')
        self.config_output_filename = os.path.join(base_path, 'data/model_vgg_config.pickle')

    def load_config(self):
        with open(self.config_output_filename, 'rb') as f_in:
            c = pickle.load(f_in)

        # turn off any data augmentation at test time
        c.use_horizontal_flips = False
        c.use_vertical_flips = False
        c.rot_90 = False

        return c

    def show_graph(self, record_df, r_epochs):
        plt.figure(figsize=(15, 5))
        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, r_epochs), record_df['mean_overlapping_bboxes'], 'r')
        plt.title('mean_overlapping_bboxes')
        plt.subplot(1, 2, 2)
        plt.plot(np.arange(0, r_epochs), record_df['class_acc'], 'r')
        plt.title('class_acc')
        plt.show()

        plt.figure(figsize=(15, 5))
        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, r_epochs), record_df['loss_rpn_cls'], 'r')
        plt.title('loss_rpn_cls')
        plt.subplot(1, 2, 2)
        plt.plot(np.arange(0, r_epochs), record_df['loss_rpn_regr'], 'r')
        plt.title('loss_rpn_regr')
        plt.show()

        plt.figure(figsize=(15, 5))
        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, r_epochs), record_df['loss_class_cls'], 'r')
        plt.title('loss_class_cls')
        plt.subplot(1, 2, 2)
        plt.plot(np.arange(0, r_epochs), record_df['loss_class_regr'], 'r')
        plt.title('loss_class_regr')
        plt.show()

        plt.figure(figsize=(15, 5))
        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, r_epochs), record_df['curr_loss'], 'r')
        plt.title('total_loss')
        plt.subplot(1, 2, 2)
        plt.plot(np.arange(0, r_epochs), record_df['elapsed_time'], 'r')
        plt.title('elapsed_time')
        plt.show()

    def format_img_size(self, img, c):
        """ formats the image size based on config """
        img_min_side = float(c.im_size)
        (height, width, _) = img.shape

        if width <= height:
            ratio = img_min_side / width
            new_height = int(ratio * height)
            new_width = int(img_min_side)
        else:
            ratio = img_min_side / height
            new_width = int(ratio * width)
            new_height = int(img_min_side)

        img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)

        return img, ratio

    def format_img_channels(self, img, c):
        """ formats the image channels based on config """
        img = img[:, :, (2, 1, 0)]
        img = img.astype(np.float32)
        img[:, :, 0] -= c.img_channel_mean[0]
        img[:, :, 1] -= c.img_channel_mean[1]
        img[:, :, 2] -= c.img_channel_mean[2]
        img /= c.img_scaling_factor
        img = np.transpose(img, (2, 0, 1))
        img = np.expand_dims(img, axis=0)

        return img

    def format_img(self, img, c):
        """ formats an image for model prediction based on config """
        img, ratio = self.format_img_size(img, c)
        img = self.format_img_channels(img, c)

        return img, ratio

    # Method to transform the coordinates of the bounding box to its original size
    def get_real_coordinates(self, ratio, x1, y1, x2, y2):
        real_x1 = int(round(x1 // ratio))
        real_y1 = int(round(y1 // ratio))
        real_x2 = int(round(x2 // ratio))
        real_y2 = int(round(y2 // ratio))

        return real_x1, real_y1, real_x2, real_y2

    """#### Measure map"""
    def get_map(self, pred, gt, f):
        t = {}
        p = {}
        fx, fy = f

        for bbox in gt:
            bbox['bbox_matched'] = False

        pred_probs = np.array([s['prob'] for s in pred])
        box_idx_sorted_by_prob = np.argsort(pred_probs)[::-1]

        for box_idx in box_idx_sorted_by_prob:
            pred_box = pred[box_idx]
            pred_class = pred_box['class']
            pred_x1 = pred_box['x1']
            pred_x2 = pred_box['x2']
            pred_y1 = pred_box['y1']
            pred_y2 = pred_box['y2']
            pred_prob = pred_box['prob']

            if pred_class not in p:
                p[pred_class] = []
                t[pred_class] = []

            p[pred_class].append(pred_prob)
            found_match = False

            for gt_box in gt:
                gt_class = gt_box['class']
                gt_x1 = gt_box['x1'] / fx
                gt_x2 = gt_box['x2'] / fx
                gt_y1 = gt_box['y1'] / fy
                gt_y2 = gt_box['y2'] / fy
                gt_seen = gt_box['bbox_matched']

                if gt_class != pred_class:
                    continue

                if gt_seen:
                    continue

                iou_map = self.rpn.iou((pred_x1, pred_y1, pred_x2, pred_y2), (gt_x1, gt_y1, gt_x2, gt_y2))

                if iou_map >= 0.5:
                    found_match = True
                    gt_box['bbox_matched'] = True
                    break
                else:
                    continue

            t[pred_class].append(int(found_match))

        for gt_box in gt:
            if not gt_box['bbox_matched']:  # and not gt_box['difficult']:
                if gt_box['class'] not in p:
                    p[gt_box['class']] = []
                    t[gt_box['class']] = []

                t[gt_box['class']].append(1)
                p[gt_box['class']].append(0)

        # import pdb
        # pdb.set_trace()
        return t, p

    def format_img_map(self, img, c):
        """Format image for map. Resize original image to c.im_size (300 in here)

        Args:
            img: cv2 image
            c: config

        Returns:
            img: Scaled and normalized image with expanding dimension
            fx: ratio for width scaling
            fy: ratio for height scaling
        """

        img_min_side = float(c.im_size)
        (height, width, _) = img.shape

        if width <= height:
            f = img_min_side / width
            new_height = int(f * height)
            new_width = int(img_min_side)
        else:
            f = img_min_side / height
            new_width = int(f * width)
            new_height = int(img_min_side)

        fx = width / float(new_width)
        fy = height / float(new_height)
        img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
        # Change image channel from BGR to RGB
        img = img[:, :, (2, 1, 0)]
        img = img.astype(np.float32)
        img[:, :, 0] -= c.img_channel_mean[0]
        img[:, :, 1] -= c.img_channel_mean[1]
        img[:, :, 2] -= c.img_channel_mean[2]
        img /= c.img_scaling_factor
        # Change img shape from (height, width, channel) to (channel, height, width)
        img = np.transpose(img, (2, 0, 1))
        # Expand one dimension at axis 0
        # img shape becames (1, channel, height, width)
        img = np.expand_dims(img, axis=0)
        return img, fx, fy

    def test(self, num_features=512):
        c = self.load_config()

        # Load the records
        record_df = pd.read_csv(c.record_path)
        r_epochs = len(record_df)

        self.show_graph(record_df, r_epochs)

        input_shape_img = (None, None, 3)
        input_shape_features = (None, None, num_features)

        img_input = Input(shape=input_shape_img)
        roi_input = Input(shape=(c.num_rois, 4))
        feature_map_input = Input(shape=input_shape_features)

        # define the base network (VGG here, can be Resnet50, Inception, etc)
        shared_layers = self.model.nn_base(img_input, trainable=True)

        # define the RPN, built on the base layers
        num_anchors = len(c.anchor_box_scales) * len(c.anchor_box_ratios)
        rpn_layers = self.rpn.rpn_layer(shared_layers, num_anchors)
        classifier = self.classifier.classifier_layer(
            feature_map_input, roi_input, c.num_rois, nb_classes=len(c.class_mapping)
        )

        model_rpn = Model(img_input, rpn_layers)
        model_classifier_only = Model([feature_map_input, roi_input], classifier)
        model_classifier = Model([feature_map_input, roi_input], classifier)

        print('Loading weights from {}'.format(c.model_path))
        model_rpn.load_weights(c.model_path, by_name=True)
        model_classifier.load_weights(c.model_path, by_name=True)

        model_rpn.compile(optimizer='sgd', loss='mse')
        model_classifier.compile(optimizer='sgd', loss='mse')

        # Switch key value for class mapping
        class_mapping = c.class_mapping
        class_mapping = {v: k for k, v in class_mapping.items()}
        print(class_mapping)
        class_to_color = {class_mapping[v]: np.random.randint(0, 255, 3) for v in class_mapping}

        test_imgs = os.listdir(self.test_base_path)
        imgs_path = []

        for i in range(12):
            idx = np.random.randint(len(test_imgs))
            imgs_path.append(test_imgs[idx])

        # If the box classification value is less than this, we ignore this box
        bbox_threshold = 0.7

        for idx, img_name in enumerate(imgs_path):
            if not img_name.lower().endswith(('.bmp', '.jpeg', '.jpg', '.png', '.tif', '.tiff')):
                continue

            print(img_name)
            st = time.time()
            filepath = os.path.join(self.test_base_path, img_name)

            img = cv2.imread(filepath)
            x, ratio = self.format_img(img, c)

            x = np.transpose(x, (0, 2, 3, 1))

            # get output layer y1, y2 from the RPN and the feature maps f
            # y1: y_rpn_cls
            # y2: y_rpn_regr
            [y1, y2, f] = model_rpn.predict(x)

            # Get bboxes by applying NMS
            # r.shape = (300, 4)
            r = self.rpn.rpn_to_roi(y1, y2, c, backend.image_dim_ordering(), overlap_thresh=0.7)

            # convert from (x1,y1,x2,y2) to (x,y,w,h)
            r[:, 2] -= r[:, 0]
            r[:, 3] -= r[:, 1]

            # apply the spatial pyramid pooling to the proposed regions
            bboxes = {}
            probs = {}

            for jk in range(r.shape[0] // c.num_rois + 1):
                rois = np.expand_dims(r[c.num_rois * jk:c.num_rois * (jk + 1), :], axis=0)
                if rois.shape[1] == 0:
                    break

                if jk == r.shape[0] // c.num_rois:
                    # pad r
                    curr_shape = rois.shape
                    target_shape = (curr_shape[0], c.num_rois, curr_shape[2])
                    rois_padded = np.zeros(target_shape).astype(rois.dtype)
                    rois_padded[:, :curr_shape[1], :] = rois
                    rois_padded[0, curr_shape[1]:, :] = rois[0, 0, :]
                    rois = rois_padded

                [p_cls, p_regr] = model_classifier_only.predict([f, rois])

                # Calculate bboxes coordinates on resized image
                for ii in range(p_cls.shape[1]):
                    # Ignore 'bg' class
                    if np.max(p_cls[0, ii, :]) < bbox_threshold or np.argmax(p_cls[0, ii, :]) == (p_cls.shape[2] - 1):
                        continue

                    cls_name = class_mapping[np.argmax(p_cls[0, ii, :])]

                    if cls_name not in bboxes:
                        bboxes[cls_name] = []
                        probs[cls_name] = []

                    (x, y, w, h) = rois[0, ii, :]

                    cls_num = np.argmax(p_cls[0, ii, :])
                    try:
                        (tx, ty, tw, th) = p_regr[0, ii, 4 * cls_num:4 * (cls_num + 1)]
                        tx /= c.classifier_regr_std[0]
                        ty /= c.classifier_regr_std[1]
                        tw /= c.classifier_regr_std[2]
                        th /= c.classifier_regr_std[3]
                        x, y, w, h = self.rpn.apply_regr(x, y, w, h, tx, ty, tw, th)
                    except:
                        pass

                    bboxes[cls_name].append([
                        c.rpn_stride * x,
                        c.rpn_stride * y,
                        c.rpn_stride * (x + w),
                        c.rpn_stride * (y + h)
                    ])
                    probs[cls_name].append(np.max(p_cls[0, ii, :]))

            all_dets = []

            for key in bboxes:
                bbox = np.array(bboxes[key])
                new_boxes, new_probs = self.rpn.non_max_suppression_fast(bbox, np.array(probs[key]), overlap_thresh=0.2)

                for jk in range(new_boxes.shape[0]):
                    (x1, y1, x2, y2) = new_boxes[jk, :]

                    # Calculate real coordinates on original image
                    (real_x1, real_y1, real_x2, real_y2) = self.get_real_coordinates(ratio, x1, y1, x2, y2)

                    cv2.rectangle(img, (real_x1, real_y1), (real_x2, real_y2), (
                    int(class_to_color[key][0]), int(class_to_color[key][1]), int(class_to_color[key][2])), 4)

                    text_label = '{}: {}'.format(key, int(100 * new_probs[jk]))
                    all_dets.append((key, 100 * new_probs[jk]))

                    (retval, baseLine) = cv2.getTextSize(text_label, cv2.FONT_HERSHEY_COMPLEX, 1, 1)
                    text_org = (real_x1, real_y1 - 0)

                    cv2.rectangle(
                        img,
                        (text_org[0] - 5, text_org[1] + baseLine - 5),
                        (text_org[0] + retval[0] + 5, text_org[1] - retval[1] - 5),
                        (0, 0, 0), 1
                    )

                    cv2.rectangle(
                        img,
                        (text_org[0] - 5, text_org[1] + baseLine - 5),
                        (text_org[0] + retval[0] + 5, text_org[1] - retval[1] - 5),
                        (255, 255, 255), -1
                    )

                    cv2.putText(img, text_label, text_org, cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)

            print('Elapsed time = {}'.format(time.time() - st))
            print(all_dets)
            plt.figure(figsize=(10, 10))
            plt.grid()
            plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
            plt.show()

        print(class_mapping)

        # This might takes a while to parser the data
        test_imgs, _, _ = Preprocessing.get_data(self.base_path, self.test_path)
        t = {}
        p = {}
        maps = []

        for idx, img_data in enumerate(test_imgs):
            print('{}/{}'.format(idx, len(test_imgs)))
            st = time.time()
            filepath = img_data['filepath']
            img = cv2.imread(filepath)
            x, fx, fy = self.format_img_map(img, c)

            # Change x (img) shape from (1, channel, height, width) to (1, height, width, channel)
            x = np.transpose(x, (0, 2, 3, 1))

            # get the feature maps and output from the RPN
            [y1, y2, f] = model_rpn.predict(x)
            r = self.rpn.rpn_to_roi(y1, y2, c, backend.image_dim_ordering(), overlap_thresh=0.7)

            # convert from (x1,y1,x2,y2) to (x,y,w,h)
            r[:, 2] -= r[:, 0]
            r[:, 3] -= r[:, 1]

            # apply the spatial pyramid pooling to the proposed regions
            bboxes = {}
            probs = {}

            for jk in range(r.shape[0] // c.num_rois + 1):
                rois = np.expand_dims(r[c.num_rois * jk:c.num_rois * (jk + 1), :], axis=0)

                if rois.shape[1] == 0:
                    break

                if jk == r.shape[0] // c.num_rois:
                    # pad r
                    curr_shape = rois.shape
                    target_shape = (curr_shape[0], c.num_rois, curr_shape[2])
                    rois_padded = np.zeros(target_shape).astype(rois.dtype)
                    rois_padded[:, :curr_shape[1], :] = rois
                    rois_padded[0, curr_shape[1]:, :] = rois[0, 0, :]
                    rois = rois_padded

                [p_cls, p_regr] = model_classifier_only.predict([f, rois])

                # Calculate all classes' bboxes coordinates on resized image (300, 400)
                # Drop 'bg' classes bboxes
                for ii in range(p_cls.shape[1]):
                    # If class name is 'bg', continue
                    if np.argmax(p_cls[0, ii, :]) == (p_cls.shape[2] - 1):
                        continue

                    # Get class name
                    cls_name = class_mapping[np.argmax(p_cls[0, ii, :])]

                    if cls_name not in bboxes:
                        bboxes[cls_name] = []
                        probs[cls_name] = []

                    (x, y, w, h) = rois[0, ii, :]
                    cls_num = np.argmax(p_cls[0, ii, :])

                    try:
                        (tx, ty, tw, th) = p_regr[0, ii, 4 * cls_num:4 * (cls_num + 1)]
                        tx /= c.classifier_regr_std[0]
                        ty /= c.classifier_regr_std[1]
                        tw /= c.classifier_regr_std[2]
                        th /= c.classifier_regr_std[3]
                        x, y, w, h = self.rpn.apply_regr(x, y, w, h, tx, ty, tw, th)
                    except:
                        pass

                    bboxes[cls_name].append([16 * x, 16 * y, 16 * (x + w), 16 * (y + h)])
                    probs[cls_name].append(np.max(p_cls[0, ii, :]))

            all_dets = []

            for key in bboxes:
                bbox = np.array(bboxes[key])

                # Apply non-max-suppression on final bboxes to get the output bounding boxe
                new_boxes, new_probs = self.rpn.non_max_suppression_fast(bbox, np.array(probs[key]), overlap_thresh=0.5)

                for jk in range(new_boxes.shape[0]):
                    (x1, y1, x2, y2) = new_boxes[jk, :]
                    det = {'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2, 'class': key, 'prob': new_probs[jk]}
                    all_dets.append(det)

            print('Elapsed time = {}'.format(time.time() - st))
            t, p = self.get_map(all_dets, img_data['bboxes'], (fx, fy))

            for key in t.keys():
                if key not in t:
                    t[key] = []
                    p[key] = []

                t[key].extend(t[key])
                p[key].extend(p[key])

            all_aps = []

            for key in t.keys():
                ap = average_precision_score(t[key], p[key])
                print('{} AP: {}'.format(key, ap))
                all_aps.append(ap)

            print('map = {}'.format(np.mean(np.array(all_aps))))
            maps.append(np.mean(np.array(all_aps)))

        print('\nmean average precision:', np.mean(np.array(maps)))

        map_filter = [map for map in maps if str(map) != 'nan']
        mean_average_prec = round(np.mean(np.array(map_filter)), 3)
        print('After training %dk batches, the mean average precision is %0.3f' % (len(record_df), mean_average_prec))

        # record_df.loc[len(record_df)-1, 'map'] = mean_average_prec
        # record_df.to_csv(c.record_path, index=0)
        # print('Save map to {}'.format(c.record_path))
