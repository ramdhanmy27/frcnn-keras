import sys

from model.TrainModel import TrainModel
from model.TestModel import TestModel

if __name__ == "__main__":
    def train(base_path):
        train_model = TrainModel(base_path)
        train_model.train()

    def test(base_path):
        test_model = TestModel(base_path)
        test_model.test()

    args = sys.argv[1:]

    {
        "test": lambda path: test(path),
        "train": lambda path: train(path),
    }.get(args[0])(args[1])
